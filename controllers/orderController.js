const mongoose = require("mongoose");
const User = require("../models/users.js");
const Course = require('../models/order.js');
const Product = require('../models/products.js');

const bcrypt = require("bcrypt");
const auth = require("../auth.js");

	module.exports.addOrder = (data) => {
		console.log(data.isAdmin)

		if(data.isAdmin) {
			let newOrder = new Order({
				userId: data.order.name,
				productsId: [
					productsId: data.order.productsId,
					quantity: data.order.quantity 
					],
				totalAmount: data.order.totalAmount, 	
				purchasedOn: data.course.purchasedOn
			});

			return newOrder.save().then((newOrder, error) => {
				if(error){
					return error
				}

				return newOrder 
			})
		};

		// If the user is not admin, then return this message as a promise to avoid errors
		let message = Promise.resolve('User must be ADMIN to access this.')

		return message.then((value) => {
			return {value}
		})
	};