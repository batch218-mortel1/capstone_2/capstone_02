const mongoose = require("mongoose");
const User = require("../models/users.js");
const Course = require('../models/order.js');
const Product = require('../models/products.js');

module.exports.addProduct = (data) => {
		console.log(data.isAdmin)

		if(data.isAdmin) {
			let newProduct = new Product({
				name: data.product.name,
				description: data.product.description,
				price: data.product.price,
				isActive : data.product.isAdmin
			});

			return newProduct.save().then((newCourse, error) => {
				if(error){
					return error
				}

				return newProduct 
			});
		};
		
		let message = Promise.resolve('User must be ADMIN to access this.')

		return message.then((value) => {
			return {value}
		})
	};

module.exports.getProduct = (productId) => {
	return Product.findById(productId).then(result => {
		return result;
	});
};

module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId , 
			{			//req.body  
				productName: newData.product.productName,
				description: newData.product.description,
				price: newData.product.price,
				isActive: newData.product.isActive,
				createdOn: newData.product.createdOn 
			}
		).then((updatedProduct, error) => {
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
	}
};

module.exports.archiveProduct = (productId) => {
	return Product.findByIdAndUpdate(productId, {
		isActive: false
	})
	.then((archivedProduct, error) => {
		if(error){
			return false
		} 

		return {
			message: "Product archived successfully!"
		}
	})
};