const mongoose = require("mongoose");
const User = require("../models/users.js");
const Course = require('../models/order.js');
const Product = require('../models/products.js');

const bcrypt = require("bcrypt");
const auth = require("../auth.js");


module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			} else	{
				return false;
			}
		}
	});
};

module.exports.getProfile = (reqBody) => {
	return User.findOne({id:reqBody.id}).then(result => {
		result.password = "";
		return result;
	})
}
