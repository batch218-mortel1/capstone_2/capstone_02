const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

router.post("/create", auth.verify, (req, res) => {

const data = {
	course: req.body,
	isAdmin: auth.decode(req.headers.authorization).isAdmin
}

userController.addUser(data).then(resultFromController => res.send(resultFromController));
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

module.exports = router;