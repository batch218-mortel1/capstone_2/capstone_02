const express = require("express");
const router = express.Router();
const userController = require("../controllers/productController.js");
const auth = require("../auth.js");

router.get("/active", (req, res) => {
	courseController.getActiveProduct().then(resultFromController => res.send(resultFromController))
});

router.get("/:productId", (req, res) => {					
	productController.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController))
});

module.exports = router;