const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const orderRoutes = require("./routes/orderRoutes.js");

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/order", orderRoutes);

mongoose.connect("mongodb+srv://Jowsefu:FordaCapstoneDos@capstonedos.cvbosln.mongodb.net/test", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to Lanestosa-Mongo DB Atlas.'));

app.listen(process.env.PORT || 5000, () => 
	{console.log(`API is now online on port ${process.env.PORT || 5000 }`)

});